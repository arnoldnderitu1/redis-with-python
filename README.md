# Example: PyHats.com
A fictional, lucrative website, PyHats.com, that sells outrageously overpriced hats to anyone who will buy them.

I use Redis to handle some of the product catalog, inventorying, and bot traffic detection on the site. 

I have a script to sell three limited-edition hats. Each hat gets held in a Redis hash of field-value pairs, and the hash has a key that is a prefixed random integer, such as `hat:56854717`. Using the `hat:` prefix is Redis convention for creating a sort of `namespace` within a Redis database.

Here is the [script](pyhats.py)